-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2018 at 11:20 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem_skripsi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(0, 'sugih', '77cbc257e66302866cf6191754c0c8e3');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `nik` varchar(20) NOT NULL,
  `nama_dosen` varchar(40) NOT NULL,
  `id_fakultas_dsn` int(11) NOT NULL,
  `id_prodi_dsn` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nohp_dsn` varchar(15) NOT NULL,
  `email_dsn` varchar(100) NOT NULL,
  `foto_dsn` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`nik`, `nama_dosen`, `id_fakultas_dsn`, `id_prodi_dsn`, `password`, `nohp_dsn`, `email_dsn`, `foto_dsn`) VALUES
('0009028307', 'Nina Sulistiyowati, S.T., M.Kom', 1063, 117, '827ccb0eea8a706c4c34a16891f84e7b', '2147483647', 'nina.sulistio@staff.unsika.ac.id', 'file_1532425389.png'),
('0023108901', 'Betha Nurina Sari, M.Kom.', 1063, 117, '827ccb0eea8a706c4c34a16891f84e7b', '2147483647', 'betha.nurina@staff.unsika.ac.id', 'file_1532426283.jpg'),
('0402047903', 'Ade Andri Hendriadi, S.Si., M.Kom', 1063, 117, '827ccb0eea8a706c4c34a16891f84e7b', '2147483647', 'ade.andri@staff.unsika.ac.id', 'file_1532426622.JPG'),
('0404128701', 'M. Jajuli, M.Si.', 1063, 117, '827ccb0eea8a706c4c34a16891f84e7b', '2147483647', 'mohamad.jajuli@staff.unsika.ac.id', 'file_1532426453.jpg'),
('182763182', 'Devi Adi', 6761257, 198298312, '27232b8582ae0f5a763c0b2f07b7cd16', '128317923', 'deanheart09@gmail.com', 'file_1527342421.png'),
('231312123321', 'Nuriansyah Hamidi', 151065, 161242, '7ddbde64a6c6ccadbc53b576fde9e54e', '08123456789', 'nurdiansyah.hamidi15102@student.unsika.ac.id', 'file_1538538348.JPG'),
('23456789876543', 'Dean Heart', 55912823, 44768887, '827ccb0eea8a706c4c34a16891f84e7b', '8766562', 'jhkahdk@gmail.com', 'file_1525655277.png'),
('5518261271', 'Ali Fikri, M.Kom', 55912823, 56127312, '827ccb0eea8a706c4c34a16891f84e7b', '876555672', 'deanheart09@gmail.com', 'file_1524881172.png'),
('564365776', 'Muhammad Bagas Gigih Y.P.S.T', 56217831, 56127312, '77cbc257e66302866cf6191754c0c8e3', '8666465', 'bagasgigi@gmail.com', 'file_1523525646.png'),
('604197405', 'Nur Ariesanto Ramdhan, M.Kom', 56217831, 561273712, '827ccb0eea8a706c4c34a16891f84e7b', '2147483647', 'nurariesanto@yahoo.com', 'file_1523525646.png'),
('92128123', 'Nur Tulus', 56217831, 56127312, '827ccb0eea8a706c4c34a16891f84e7b', '2147483647', 'deanheart9@gmail.com', 'file_1524314216.png');

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `id_fakultas` bigint(20) NOT NULL,
  `fakultas` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id_fakultas`, `fakultas`) VALUES
(1063, 'Ilmu Komputer'),
(1064, 'Pertanian'),
(132123, 'Joko'),
(151065, 'Ilmu Keguruan dan Ilmu Pemerintahan'),
(6761257, 'Ekonomi'),
(55912823, 'Pendidikan'),
(56217831, 'Teknik');

-- --------------------------------------------------------

--
-- Table structure for table `ide_skripsi`
--

CREATE TABLE `ide_skripsi` (
  `id_ide` bigint(20) NOT NULL,
  `nim_mhs_ide` bigint(20) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggal` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `konsultasi`
--

CREATE TABLE `konsultasi` (
  `id_konsultasi` int(11) NOT NULL,
  `nim_mhs_ks` bigint(30) NOT NULL,
  `pembimbing` varchar(30) NOT NULL,
  `catatan` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsultasi`
--

INSERT INTO `konsultasi` (`id_konsultasi`, `nim_mhs_ks`, `pembimbing`, `catatan`, `tanggal`) VALUES
(328, 55201140012, 'Muhammad Bagas Gigih Y.P.S.T', 'Revisi Bab 1', '2018-06-06'),
(329, 1610631170135, 'Betha Nurina Sari, M.Kom.', 'dek lilis ngerjain nya yang bener yaaaa', '2018-07-24'),
(330, 1610631170135, 'Betha Nurina Sari, M.Kom.', 'bab 2 nya ada yang salah', '2018-07-24'),
(331, 1510631170013, 'M. Jajuli, M.Si.', 'kamu jelek', '2018-10-03'),
(332, 1510631170013, 'M. Jajuli, M.Si.', 'jelek semua', '2018-10-03');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` bigint(20) NOT NULL,
  `nama_mhs` varchar(30) NOT NULL,
  `pwd_mhs` varchar(200) NOT NULL,
  `id_fakultas_mhs` bigint(20) NOT NULL,
  `id_prodi_mhs` int(20) NOT NULL,
  `id_skripsi_mhs` int(11) NOT NULL,
  `nohp_mhs` varchar(20) NOT NULL,
  `email_mhs` varchar(100) NOT NULL,
  `foto_mhs` varchar(30) NOT NULL,
  `QR_Code` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama_mhs`, `pwd_mhs`, `id_fakultas_mhs`, `id_prodi_mhs`, `id_skripsi_mhs`, `nohp_mhs`, `email_mhs`, `foto_mhs`, `QR_Code`, `status`) VALUES
(55201140012, 'Devi Adi Nufriana', '77cbc257e66302866cf6191754c0c8e3', 56217831, 56127312, 1528299403, '085642612283', 'deanheart09@gmail.com', 'file_1527257619.png', '55201140012.png', 'Alumni'),
(55201140013, 'Waluyo Eka', '', 6761257, 198298312, 0, '0856728392', 'ekawaluyo@gmail.com', 'file_1528257545.jpg', '', 'Mahasiswa'),
(55201140014, 'Nia Vera', '', 56217831, 561273712, 0, '085267126', 'niavera@gmail.com', 'file_1528258375.jpg', '', 'Mahasiswa'),
(55201140015, 'Solihah', '', 55912823, 44768887, 0, '0852671223', 'solihah@gmail.com', 'file_1528258415.jpg', '', 'Mahasiswa'),
(1510631170013, 'Ahmad Komarudin', '9a362a91b4de4a313bed73647479200c', 1063, 117, 1532945690, '08999898998', 'ahmad.komarudin15013@student.u', 'file_1532945083.jpg', '', 'Mahasiswa'),
(1510631170102, 'Nurdiansyah Hamidi', '7ddbde64a6c6ccadbc53b576fde9e54e', 1063, 117, 0, '081297416241', 'nurdiansyah.hamidi15102@studen', 'file_1538538542.jpg', '', 'Mahasiswa'),
(1510631170134, 'Septian Ilham', '5b3bb3e5458e02aa356f2fc671ae08d9', 1063, 117, 1532427333, '0899898989', 'septian.ilham15134@student.uns', 'file_1532426973.jpg', '', 'Mahasiswa'),
(1510631170140, 'Sugih Pratama', 'fbcc66cb8b819f2f2a5ba3644e96b5b1', 1063, 117, 1532426074, '082258579714', 'sugih.pratama15140@student.uns', 'file_1532425889.jpg', '', 'Mahasiswa'),
(1510631170144, 'Reza Arab', 'bb98b1d0b523d5e783f931550d7702b6', 1063, 117, 0, '0878787878', 'reza.arab@gmail.com', 'file_1542869048.jpg', '', 'Mahasiswa'),
(1610631170135, 'Lilis Oktianingrum', 'b4c7848b06d83bbca966b1fd05cfabf8', 1063, 117, 1532427455, '089898989898', 'lilis.16135@student.unsika.ac.', 'file_1532427069.jpg', '', 'Alumni');

-- --------------------------------------------------------

--
-- Table structure for table `metlit`
--

CREATE TABLE `metlit` (
  `id` int(11) NOT NULL,
  `id_fakultas_prodi` int(11) NOT NULL,
  `nik_dosen_metlit` varchar(20) NOT NULL,
  `prodi` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metlit`
--

INSERT INTO `metlit` (`id`, `id_fakultas_prodi`, `nik_dosen_metlit`, `prodi`) VALUES
(1, 1063, '0023108901', 'Teknik Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `id` int(11) NOT NULL,
  `pemberitahuan` varchar(300) NOT NULL,
  `catatan` text NOT NULL,
  `tanggal` varchar(40) NOT NULL,
  `tempat` varchar(30) NOT NULL,
  `ruangan` varchar(10) NOT NULL,
  `jam` varchar(10) NOT NULL,
  `jenis_kegiatan` varchar(30) NOT NULL,
  `penguji1` varchar(40) NOT NULL,
  `penguji2` varchar(40) NOT NULL,
  `penerima` bigint(20) NOT NULL,
  `pengirim` varchar(50) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemberitahuan`
--

INSERT INTO `pemberitahuan` (`id`, `pemberitahuan`, `catatan`, `tanggal`, `tempat`, `ruangan`, `jam`, `jenis_kegiatan`, `penguji1`, `penguji2`, `penerima`, `pengirim`, `status`) VALUES
(89, 'Rancang Bangun Sistem Informasi Skripsi Berbasis Web di Universitas Muhadi Setiabudi', 'Lanjutkan Proposal !', '2018-06-06', '', '', '', '', '', '', 55201140012, '564365776', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(90, 'Rancang Bangun Sistem Informasi Skripsi Berbasis Web di Universitas Muhadi Setiabudi', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Devi Adi Nufriana<br> Silahkan Lihat Di Data Skripsi', '2018-06-06', '', '', '', '', '', '', 92128123, '564365776', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(91, 'Rancang Bangun Sistem Informasi Skripsi Berbasis Web di Universitas Muhadi Setiabudi', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Devi Adi Nufriana<br> Silahkan Lihat Di Data Skripsi', '2018-06-06', '', '', '', '', '', '', 564365776, '564365776', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(92, 'Proposal Rancang Bangun Sistem Informasi Skripsi Berbasis Web di Universitas Muhadi Setiabudi Telah Di ACC', 'Proposal Telah Di ACC Oleh : <br>Muhammad Bagas Gigih Y.P.S.T Sebagai Pembimbing 2', '2018-06-06', '', '', '', '', '', '', 55201140012, '564365776', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i> Proposal </span>'),
(93, 'Kegiatan Seminar Proposal Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i>  12:40<br> <i class=\"fas fa-map-marker mr-auto\"></i>  Tegal<br> <i class=\"fas fa-calendar-alt\"></i> Senin, 04 Juni 2018', '2018-06-06', '', '', '', '', '', '', 55201140012, '564365776', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Seminar Proposal</span>'),
(94, 'PENERAPAN ALGORITMA DECISION TREE C4.5 UNTUK PENILAIAN RUMAH TINGGAL', 'Ini adalah catatan untuk mahasiswa', '2018-07-24', '', '', '', '', '', '', 1610631170135, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(95, 'PENERAPAN ALGORITMA DECISION TREE C4.5 UNTUK PENILAIAN RUMAH TINGGAL', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Lilis Oktianingrum<br> Silahkan Lihat Di Data Skripsi', '2018-07-24', '', '', '', '', '', '', 23108901, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(96, 'PENERAPAN ALGORITMA DECISION TREE C4.5 UNTUK PENILAIAN RUMAH TINGGAL', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Lilis Oktianingrum<br> Silahkan Lihat Di Data Skripsi', '2018-07-24', '', '', '', '', '', '', 402047903, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(97, 'Proposal PENERAPAN ALGORITMA DECISION TREE C4.5 UNTUK PENILAIAN RUMAH TINGGAL Telah Di ACC', 'Proposal Telah Di ACC Oleh : <br>Betha Nurina Sari, M.Kom. Sebagai Pembimbing 1', '2018-07-24', '', '', '', '', '', '', 1610631170135, '0023108901', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i> Proposal </span>'),
(105, 'Penerapan Algoritma Klasifikasi C4.5 dalam. Rekomendasi Penerimaan Mitra Penjualan', 'jangan copas ya', '2018-07-25', '', '', '', '', '', '', 1510631170134, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(106, 'Penerapan Algoritma Klasifikasi C4.5 dalam. Rekomendasi Penerimaan Mitra Penjualan', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Septian Ilham<br> Silahkan Lihat Di Data Skripsi', '2018-07-25', '', '', '', '', '', '', 402047903, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(107, 'Penerapan Algoritma Klasifikasi C4.5 dalam. Rekomendasi Penerimaan Mitra Penjualan', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Septian Ilham<br> Silahkan Lihat Di Data Skripsi', '2018-07-25', '', '', '', '', '', '', 23108901, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(108, 'Penerapan Sistem Skripsi Online Pada Universitas Singaperbangsa Karawang', 'oke deh', '2018-07-25', '', '', '', '', '', '', 1510631170140, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(109, 'Penerapan Sistem Skripsi Online Pada Universitas Singaperbangsa Karawang', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Sugih Pratama<br> Silahkan Lihat Di Data Skripsi', '2018-07-25', '', '', '', '', '', '', 9028307, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(110, 'Penerapan Sistem Skripsi Online Pada Universitas Singaperbangsa Karawang', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Sugih Pratama<br> Silahkan Lihat Di Data Skripsi', '2018-07-25', '', '', '', '', '', '', 404128701, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(114, 'Kegiatan Seminar Proposal Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i> Jam      : 08:00<br> <i class=\"fas fa-map-marker mr-auto\"></i> Tempat   : Fakultas Ilmu Komputer<br> <i class=\"fa fa-building\"></i> Ruangan  : F1<br> <i class=\"fas fa-calendar-alt\"></i> Tanggal : Selasa, 31 Juli 2018<br> <i class=\"fa fa-user\"></i> Penguji1 : Betha Nurina Sari, M.Kom.', '2018-07-25', 'Fakultas Ilmu Komputer', 'F1', '08:00', 'Seminar Proposal', 'Betha Nurina Sari, M.Kom.', '', 1510631170140, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Seminar Proposal</span>'),
(115, 'Kegiatan Seminar Proposal Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i> Jam      : 10:00<br> <i class=\"fas fa-map-marker mr-auto\"></i> Tempat   : Fakultas Ilmu Komputer<br> <i class=\"fa fa-building\"></i> Ruangan  : F1<br> <i class=\"fas fa-calendar-alt\"></i> Tanggal : Selasa, 31 Juli 2018<br> <i class=\"fa fa-user\"></i> Penguji1 : Ade Andri Hendriadi, S.Si., M.Kom', '2018-07-25', 'Fakultas Ilmu Komputer', 'F1', '10:00', 'Seminar Proposal', 'Ade Andri Hendriadi, S.Si., M.Kom', '', 1510631170134, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Seminar Proposal</span>'),
(116, 'Kegiatan Seminar Proposal Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i> Jam      : 12:00<br> <i class=\"fas fa-map-marker mr-auto\"></i> Tempat   : Fakultas Ilmu Komputer<br> <i class=\"fa fa-building\"></i> Ruangan  : F1<br> <i class=\"fas fa-calendar-alt\"></i> Tanggal : Selasa, 31 Juli 2018<br> <i class=\"fa fa-user\"></i> Penguji1 : M. Jajuli, M.Si.', '2018-07-25', 'Fakultas Ilmu Komputer', 'F1', '12:00', 'Seminar Proposal', 'M. Jajuli, M.Si.', '', 1610631170135, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Seminar Proposal</span>'),
(125, 'Kegiatan Sidang Kolokium Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i> Jam      : 09:00<br> <i class=\"fas fa-map-marker mr-auto\"></i> Tempat   : Fasilkom<br> <i class=\"fa fa-building\"></i> Ruangan  : F2<br> <i class=\"fas fa-calendar-alt\"></i> Tanggal : Sabtu, 04 Agustus 2018<br> <i class=\"fa fa-user\"></i> Penguji1 : Betha Nurina Sari, M.Kom.', '2018-07-27', 'Fasilkom', 'F2', '09:00', 'Sidang Kolokium', 'Betha Nurina Sari, M.Kom.', 'M. Jajuli, M.Si.', 1510631170140, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Seminar Proposal</span>'),
(126, 'Kegiatan Sidang Yudisium Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i> Jam      : 12:00<br> <i class=\"fas fa-map-marker mr-auto\"></i> Tempat   : Fasilkom<br> <i class=\"fa fa-building\"></i> Ruangan  : F4<br> <i class=\"fas fa-calendar-alt\"></i> Tanggal : Sabtu, 04 Agustus 2018<br> <i class=\"fa fa-user\"></i> Penguji1 : ', '2018-07-27', 'Fasilkom', 'F4', '12:00', 'Sidang Yudisium', '', '', 1610631170135, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Seminar Proposal</span>'),
(130, 'Proposal Penerapan Sistem Skripsi Online Pada Universitas Singaperbangsa Karawang Telah Di ACC', 'Proposal Telah Di ACC Oleh : <br>Nina Sulistiyowati, S.T., M.Kom Sebagai Pembimbing 1', '2018-07-29', '', '', '', '', '', '', 1510631170140, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i> Proposal </span>'),
(131, 'Proposal Penerapan Sistem Skripsi Online Pada Universitas Singaperbangsa Karawang Telah Di ACC', 'Proposal Telah Di ACC Oleh : <br>M. Jajuli, M.Si. Sebagai Pembimbing 2', '2018-07-29', '', '', '', '', '', '', 1510631170140, '0404128701', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i> Proposal </span>'),
(132, 'Skripsi Penerapan Sistem Skripsi Online Pada Universitas Singaperbangsa Karawang Telah Di ACC', 'Skripsi Telah Di ACC Oleh : <br>M. Jajuli, M.Si.<br> Sebagai Pembimbing 2', '2018-07-29', '', '', '', '', '', '', 1510631170140, '0404128701', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i> Skripsi </span>'),
(145, 'Kegiatan Sidang Yudisium Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i>  09:00<br> <i class=\"fas fa-map-marker mr-auto\"></i>  Fasilkom<br> <i class=\"fa fa-building\"></i> F1<br> <i class=\"fas fa-calendar-alt\"></i> Jumat, 27 Juli 2018', '2018-07-27', 'Fasilkom', 'F1', '09:00', 'Sidang Yudisium', '', '', 1510631170134, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Sidang Yudisium</span>'),
(146, 'Kegiatan Sidang Yudisium Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i>  15:00<br> <i class=\"fas fa-map-marker mr-auto\"></i>  Fasilkom<br> <i class=\"fa fa-building\"></i> F1<br> <i class=\"fas fa-calendar-alt\"></i> Jumat, 27 Juli 2018', '2018-07-27', 'Fasilkom', 'F1', '15:00', 'Sidang Yudisium', '', '', 1510631170140, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Sidang Yudisium</span>'),
(149, 'Aplikasi algoritma hough transform untuk ekstraksi fitur-fitur penting pada wajah', 'asdasdsda', '2018-10-03', '', '', '', '', '', '', 1510631170013, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(150, 'Aplikasi algoritma hough transform untuk ekstraksi fitur-fitur penting pada wajah', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Ahmad Komarudin<br> Silahkan Lihat Di Data Skripsi', '2018-10-03', '', '', '', '', '', '', 404128701, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(151, 'Aplikasi algoritma hough transform untuk ekstraksi fitur-fitur penting pada wajah', 'Anda Di Tetapkan Sebagai Dosen Pembimbing <br>Ahmad Komarudin<br> Silahkan Lihat Di Data Skripsi', '2018-10-03', '', '', '', '', '', '', 23108901, '0009028307', '<span class=\"text-right badge badge-success\"> <i class=\"fas fa-thumbs-up\"></i> Diterima </span>'),
(152, 'Proposal Aplikasi algoritma hough transform untuk ekstraksi fitur-fitur penting pada wajah Telah Di ACC', 'Proposal Telah Di ACC Oleh : <br>M. Jajuli, M.Si. Sebagai Pembimbing 1', '2018-10-03', '', '', '', '', '', '', 1510631170013, '0404128701', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i> Proposal </span>'),
(153, 'Skripsi Aplikasi algoritma hough transform untuk ekstraksi fitur-fitur penting pada wajah Telah Di ACC', 'Skripsi Telah Di ACC Oleh : <br>M. Jajuli, M.Si.<br> Sebagai Pembimbing 1', '2018-10-03', '', '', '', '', '', '', 1510631170013, '0404128701', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i> Skripsi </span>'),
(154, 'Kegiatan Seminar Proposal Telah Ditetapkan', '<i class=\"fas fa-clock mr-auto\"></i> Jam      : 09:00<br> <i class=\"fas fa-map-marker mr-auto\"></i> Tempat   : Karawang<br> <i class=\"fa fa-building\"></i> Ruangan  : R.F1<br> <i class=\"fas fa-calendar-alt\"></i> Tanggal : Rabu, 31 Oktober 2018<br> <i class=\"fa fa-user\"></i> Penguji1 : Ade Andri Hendriadi, S.Si., M.Kom', '2018-10-31', 'Karawang', 'R.F1', '09:00', 'Seminar Proposal', 'Ade Andri Hendriadi, S.Si., M.Kom', '', 1510631170013, '0009028307', '<span class=\"text-right badge badge-info\"> <i class=\"fas fa-info\"></i>Seminar Proposal</span>');

-- --------------------------------------------------------

--
-- Table structure for table `pembimbing`
--

CREATE TABLE `pembimbing` (
  `id_pmb` int(11) NOT NULL,
  `nim_mhs_pmb` bigint(20) NOT NULL,
  `nik_dsn_pmb` varchar(11) NOT NULL,
  `id_skripsi_pmb` int(11) NOT NULL,
  `status_proposal` varchar(20) NOT NULL,
  `status_skripsi` varchar(20) NOT NULL,
  `level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembimbing`
--

INSERT INTO `pembimbing` (`id_pmb`, `nim_mhs_pmb`, `nik_dsn_pmb`, `id_skripsi_pmb`, `status_proposal`, `status_skripsi`, `level`) VALUES
(3, 55201140012, '92128123', 1528299403, 'Belum Disetujui', 'Belum Disetujui', 'Pembimbing 1'),
(4, 55201140012, '564365776', 1528299403, 'Disetujui', 'Belum Disetujui', 'Pembimbing 2'),
(5, 1610631170135, '23108901', 1532427455, 'Disetujui', 'Belum Disetujui', 'Pembimbing 1'),
(6, 1610631170135, '402047903', 1532427455, 'Belum Disetujui', 'Belum Disetujui', 'Pembimbing 2'),
(7, 1510631170134, '402047903', 1532427333, 'Belum Disetujui', 'Belum Disetujui', 'Pembimbing 1'),
(8, 1510631170134, '23108901', 1532427333, 'Belum Disetujui', 'Belum Disetujui', 'Pembimbing 2'),
(9, 1510631170140, '0009028307', 1532426074, 'Disetujui', 'Belum Disetujui', 'Pembimbing 1'),
(10, 1510631170140, '0404128701', 1532426074, 'Disetujui', 'Disetujui', 'Pembimbing 2'),
(11, 1510631170013, '0404128701', 1532945690, 'Disetujui', 'Disetujui', 'Pembimbing 1'),
(12, 1510631170013, '0023108901', 1532945690, 'Belum Disetujui', 'Belum Disetujui', 'Pembimbing 2');

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `id` int(11) NOT NULL,
  `id_fakultas_prd` int(11) NOT NULL,
  `nik_kaprodi` varchar(20) NOT NULL,
  `prodi` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`id`, `id_fakultas_prd`, `nik_kaprodi`, `prodi`) VALUES
(117, 1063, '0009028307', 'Teknik Informatika'),
(118, 1064, '', 'Agroteknologi'),
(161242, 151065, '', 'Pendidikan Matematika'),
(762612, 55912823, '5518261271', 'Bahasa Indonesia'),
(7261521, 871268212, '0', 'Teknologi Pangan'),
(7261527, 871268212, '0', 'Agri Bisnis'),
(44768887, 55912823, '23456789876543', 'Guru Sekolah Dasar'),
(56127312, 56217831, '564365776', 'Informatika'),
(198298312, 6761257, '92128123', 'Akutansi'),
(561273712, 56217831, '604197405', 'Sipil');

-- --------------------------------------------------------

--
-- Table structure for table `skripsi`
--

CREATE TABLE `skripsi` (
  `id_skripsi` int(20) NOT NULL,
  `nim_mhs_skripsi` bigint(20) NOT NULL,
  `judul_skripsi` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggal` varchar(200) NOT NULL,
  `nilai` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skripsi`
--

INSERT INTO `skripsi` (`id_skripsi`, `nim_mhs_skripsi`, `judul_skripsi`, `deskripsi`, `tanggal`, `nilai`) VALUES
(1528299403, 55201140012, 'Rancang Bangun Sistem Informasi Skripsi Berbasis Web di Universitas Muhadi Setiabudi', 'Proses skripsi menjadi lebih teratur dan cepat yang dilakukan secara online yang bisa diakses dimana saja melalui browser dengan bertujuan menghemat waktu, tenaga dan memudahkan mendapatkan informasi proses skripsi secara Online.', '2018-06-06', 0),
(1532426074, 1510631170140, 'Penerapan Sistem Skripsi Online Pada Universitas Singaperbangsa Karawang', 'Ini adalah deskripsi dari skripsi yang akan saya buat dengan menggunakan  metode anu. sementara saya membuat skripsi terima kasih kepada seluruh pihak yang membantu dan lain lain. Dan masih banyak lagi ', '2018-07-25', 0),
(1532427333, 1510631170134, 'Penerapan Algoritma Klasifikasi C4.5 dalam. Rekomendasi Penerimaan Mitra Penjualan', 'Permasalahan yang sering muncul dalam bisnis pada penjualan dengan\r\nsistem pembayaran kredit tempo adalah antara lain seperti kredit macet, order\r\nfiktif dan penipuan. Jika tidak menggunakan prosedur yang benar dalam\r\npenerimaan mitra penjualan bukan tidak mungkin masalah tersebut akan\r\ntimbul dalam proses bisnis. Untuk mengembangkan bisnis tentu perlu\r\nmeningkatkan penjualan dan dibarengi dengan pembayaran yang lancar. Oleh\r\nkarena itu, pada proses penerimaan mitra perlu dibuatkan prosedur yang baik\r\nagar dapat meminimalisir kemungkinan masalah yang ada. Pada prosedur\r\npenerimaan mitra penjualan perlu dilakukan proses analisa untuk menentukan\r\ncalon mitra penjualan tersebut diterima atau ditolak. Dari data mitra yang ada\r\ndapat digunakan untuk dilakukan pengolahan data. Salah satu teknik\r\npengolahan data yang dapat digunakan dalam proses tersebut adalah\r\nklasifikasi. Klasifikasi adalah teknik pengolahan data yang membagi objek\r\nmenjadi beberapa kelas sesuai dengan jumlah kelas yang diinginkan. Dan\r\nmenggunakan Algoritma C4.5 dalam proses pengklasifikasi-annya. Yang\r\ndapat menentukan apakah calon mitra dapat diterima sebagai mitra atau\r\ntidak. Kemudian, Hasil dari pengklasifikasiannya divalidasi dengan ten-fold\r\ncross validation dengan tingkat akurasi 96,26 %, presisi 100% dan recall\r\n71,43%.', '2018-07-25', 0),
(1532427455, 1610631170135, 'PENERAPAN ALGORITMA DECISION TREE C4.5 UNTUK PENILAIAN RUMAH TINGGAL', 'Kredit bermasalah di perbankan dapat disebabkan oleh berbagai hal apakah\r\nitu kesengajaan pihak yang terlibat dalam proses kredit atau kesalahan prosedur\r\natau mungkin faktor makro ekonomi.\r\nUntuk dapat mencari permasalahan tersebut yang pertama dilihat tentu saja\r\nadalah prosedur atau proses kredit tersebut dilaksanakan dengan benar atau ada\r\npenyimpangan. Apabila prosedur telah dijalankan dengan benar maka perlu dikaji\r\napakah ada kesengajaan pihak-pihak yang terlibat dalam memberikan\r\nrekomendasi, opini dan informasi yang tidak sebenarnya. Atau mungkin perlu ', '2018-07-24', 90),
(1532945690, 1510631170013, 'Aplikasi algoritma hough transform untuk ekstraksi fitur-fitur penting pada wajah', 'Skripsi adalah istilah yang digunakan di Indonesia untuk mengilustrasikan suatu karya tulis ilmiah berupa paparan tulisan hasil penelitian sarjana S1 yang membahas suatu permasalahan/fenomena dalam bidang ilmu tertentu dengan menggunakan kaidah-kaidah yang berlaku. Skripsi merupakan karya tulis ilmiah berdasarkan hasil penelitian lapangan dan atau studi kepustakaan yang disusun mahasiswa sesuai dengan bidang studinya sebagai tugas akhir. Jadi intinya adalah Skripsi merupakan Persyaratan terakhir seorang mahasiswa untuk mencapai Gelar S1 sesuai bidang keahliannya.\r\n\r\nMenyelesaikan skripsi memang membutuhkan banyak energi, harus korban waktu, tenaga buat kejar-kejar tanda tangan dan asistensi dosen, korban perasaan jika skripsi yang dikerjakan sampai begadang ternyata dicorat-coret begitu saja sama dosen pembimbing, tapi itulah suka duka nya menjadi mahasiswa akhir. Nah, sebelum mengalami semua itu langkah pertama yang menjadi faktor penghambat adalah pemilihan judul yang akan dijadikan skripsi. Di bagian ini biasanya mahasiswa akhir sangat bingung dalam menentukan judul skripsi padahal banyak hal yang bisa jadi inspirasi buat judul skripsi.', '2018-10-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `token`, `user_id`, `created`) VALUES
(15, 'cb29ef256cdf704e0a2d06089cadd3', 2147483647, '2018-04-28'),
(16, 'd5496f0eee6ece4aef333b96ea2a8b', 2147483647, '2018-04-28'),
(17, 'd216cfb9e053abe808427c22ba7c7d', 2147483647, '2018-04-28'),
(18, 'c02f16b65ab8275dbe0ad30a9cea1d', 2147483647, '2018-04-28'),
(19, '245a84948031edfb1e7537356f4ccb', 2147483647, '2018-04-28'),
(20, '61ce856087d06ced37a9b0fd817181', 2147483647, '2018-04-28'),
(21, '48def2c8ea27e61ac462b37323b9e5', 23424324, '2018-04-28'),
(22, '10d7df726a33d7dd2b801083560566', 552011400123, '2018-04-29'),
(23, 'b41e6fa06e302d26e06e0995a0235a', 552011400123, '2018-04-29'),
(24, '1060c58543e283a8e4836d576f3594', 552011400123, '2018-04-29'),
(25, '24529948f9a6b5b2b2a79d549f128a', 552011400123, '2018-04-29'),
(26, '4ad289d33fdbc46366083e6ed6d8db', 552011400123, '2018-04-29'),
(27, 'd4cf7d41faa71bbc9cbe62e3cc2687', 552011400123, '2018-04-29'),
(28, '6c95fbde6582435f56d7aa1f752fc5', 552011400123, '2018-04-29'),
(29, '65ccba0dc0797b93da4d0d0544d00c', 552011400123, '2018-04-29'),
(30, '683450a2a470125d297704fd4d35e2', 552011400123, '2018-04-29'),
(31, 'c912791cd3555910eafacb895423c2', 552011400123, '2018-04-29'),
(32, 'ec76ec73e0b692c27588b5ccee9c46', 552011400123, '2018-04-29'),
(33, '388e9b0b5619c7919cf798298cf0e8', 552011400123, '2018-04-29'),
(34, '071a20c9cde59c92ec8446807f4201', 552011400123, '2018-04-29'),
(35, '0786faa17e2d55c26a24b59b51d63d', 552011400123, '2018-04-29'),
(36, '27265062e0ed463e26d49bc3d542c6', 552011400123, '2018-04-29'),
(37, '24896ef117f20f61ea160dded89d14', 552011400123, '2018-04-29'),
(38, '440e39952ad3dbb3e495dcee1086c3', 552011400123, '2018-04-29'),
(39, '20c14a2678018ea84f7cf523fb378e', 552011400123, '2018-04-29'),
(40, '4f9445d59c8f7a2f2ac9e3805d6b9d', 552011400123, '2018-04-29'),
(41, 'd12fee69155ea852693140fcdb4a1b', 55201140012, '2018-05-25'),
(42, '87e80718c661f99d0bbe6369c27007', 55201140012, '2018-05-25'),
(43, '356b6eeaea4a3a9d267e5d15e12f45', 55201140012, '2018-05-25'),
(44, '45268970710e946fd1e25584a88fab', 55201140012, '2018-05-25'),
(45, 'e3a3b9de42db9d3632780c7941fec7', 55201140012, '2018-05-25'),
(46, 'cf394fec07008a70127c0401ad73fa', 55201140012, '2018-05-25'),
(47, 'ad0bcba23361d9504d448b7359666e', 55201140012, '2018-05-26'),
(48, '50d3a7cde9b021fcccd00a4443488a', 55201140012, '2018-05-26'),
(49, 'd340c0c6b83b489bbc7f2dacd0aabf', 55201140012, '2018-05-26'),
(50, 'a94e8a2bf56923c051bdd1e64efbcf', 55201140012, '2018-05-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD UNIQUE KEY `id_admin` (`id_admin`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indexes for table `ide_skripsi`
--
ALTER TABLE `ide_skripsi`
  ADD PRIMARY KEY (`id_ide`);

--
-- Indexes for table `konsultasi`
--
ALTER TABLE `konsultasi`
  ADD PRIMARY KEY (`id_konsultasi`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`),
  ADD KEY `id_skripsi_mhs` (`id_skripsi_mhs`),
  ADD KEY `id_jurusan_mhs` (`id_fakultas_mhs`),
  ADD KEY `id_konsentrasi_mhs` (`id_prodi_mhs`);

--
-- Indexes for table `metlit`
--
ALTER TABLE `metlit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembimbing`
--
ALTER TABLE `pembimbing`
  ADD PRIMARY KEY (`id_pmb`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skripsi`
--
ALTER TABLE `skripsi`
  ADD PRIMARY KEY (`id_skripsi`),
  ADD KEY `nim_mhs_skripsi` (`nim_mhs_skripsi`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `konsultasi`
--
ALTER TABLE `konsultasi`
  MODIFY `id_konsultasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=333;

--
-- AUTO_INCREMENT for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `pembimbing`
--
ALTER TABLE `pembimbing`
  MODIFY `id_pmb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=561273713;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
