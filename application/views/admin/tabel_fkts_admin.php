<head>
	
</head>
<div id="container" class="row">
	<div class="col-md table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th scope="col">ID Fakultas</th>
					<th scope="col">Fakultas</th>
					<th><i class="fas fa-spinner fa-pulse loading" style="display: none"> </i> 
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($fakultas as $j) {
					?>
					<tr class="tabel<?php echo $j->id_fakultas?>">
						<th scope="row"> <?php echo $j->id_fakultas;?></th>
						<td> <a id="fakultas" class="btn_view" href="<?php echo base_url('Admin/tabel_prodi_admin/').$j->id_fakultas;?>"> <?php echo $j->fakultas?> </a> </td>
						<td><a id="<?php echo $j->id_fakultas;?>" href="<?php echo base_url('Admin/delete_fakultas/').$j->id_fakultas;?>" class="float-right hapus"><i class="fas fa-trash"> </i></a></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="col-md-8 SHfakultas" style="display: none">
			<div id="SHfakultas">
				
			</div>
		</div>
	</div>