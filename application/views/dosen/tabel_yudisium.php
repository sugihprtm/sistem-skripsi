<head>
	<script type="text/javascript" src="<?php echo base_url('assets/js/myscript.js');?>"></script>
	<script type="text/javascript">
		$(function(){

			$(document).on("click","div.small",function(){
				$(this).find("span[class~='caption']").hide();
				$(this).find("input[class~='editor']").fadeIn().focus();
			});

	</script>
</head>
<div class="table-responsive">
	<table class="table">
		<thead>
		<tr>
			<th scope="col">NPM</th>
			<th scope="col">Nama</th>
			<th scope="col">Tanggal</th>
			<th scope="col">Tempat</th>
			<th scope="col">Ruangan</th>
			<th scope="col">Jam</th>
			
		</tr>
		</thead>
		<tbody>
			<?php if (!empty($pemberitahuan)): foreach ($pemberitahuan as $u):
				
				?>
				<tr class="text-left list-item">
					<td><?php echo $u['nim'];?></td>
					<td><a href="<?php echo base_url('Kaprodi/profil_mhs/'.$u['nim']);?>"><?php echo $u['nama_mhs'];?></a></td> 
					<td><?php echo date('d F Y', strtotime($u['tanggal']));?></td>
					<td><?php echo $u['tempat'];?></td>
					<td><?php echo $u['ruangan'];?></td>
					<td><?php echo $u['jam'];?></td>
					
				</tr>
				
			<?php endforeach; else: ?>
		
		</tbody>
	</table>
	<div class="col-md text-center">
		<p>Belum Ada Data.</p>	
	</div>
</div>

<?php endif; ?>
</div>