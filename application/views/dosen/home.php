<head>
	<script type="text/javascript">    
		jQuery(document).ready(function($) {
			$('#tabel_skripsi').load('<?php echo base_url('dosen/tabel_skripsi') ;?>')	
			$('#pemberitahuan').load('<?php echo base_url('dosen/pemberitahuan') ;?>')
			$('#tabel_seminar_proposal').load('<?php echo base_url('dosen/tabel_seminar_proposal');?>');
			$('#tabel_kolokium').load('<?php echo base_url('dosen/tabel_kolokium');?>');
			$('#tabel_yudisium').load('<?php echo base_url('dosen/tabel_yudisium');?>');
			$('#profil').load('<?php echo base_url('dosen/profil') ;?>')
			$('.bars').toggle('slow');	
			$(".btn-menu").click(function() {
				$("#profil").toggle('slow');
			});
		});
		
	</script>
</head>
<body>
	
	<div class="container-fluid">
		<div>
			<div class="nav nav-pills flex-column flex-sm-row" id="list-tab" role="tablist">
				<a href="#" class="nav-link btn-menu"> <i class="fas fa-bars"> </i> </a>
				<a class="nav-item nav-link active" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings"> <i class="fas fa-envelope"></i> Pemberitahuan</a>
				<a class="nav-item nav-link" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home"> <i class="fas fa-users"></i> Skripsi </a>
				<a class="nav-link nav-item" id="list-proposal-list" data-toggle="list" href="#list-proposal" role="tab" aria-controls="settings"> <i class="far fa-calendar-alt"></i> Jadwal Seminar Proposal </a>
				<a class="nav-link nav-item" id="list-kolokium-list" data-toggle="list" href="#list-kolokium" role="tab" aria-controls="settings"> <i class="far fa-calendar-alt"></i> Jadwal Sidang Kolokium </a>
				<a class="nav-link nav-item" id="list-yudisium-list" data-toggle="list" href="#list-yudisium" role="tab" aria-controls="settings"> <i class="far fa-calendar-alt"></i> Jadwal Sidang Yudisium </a>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-3" id="profil">

				</div>
				<div class="col-md">
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
							<div class="scroll">
								<div id="pemberitahuan">

								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
							<div class="scroll">
								<div id="tabel_skripsi">

								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="list-proposal" role="tabpanel" aria-labelledby="list-proposal-list">
							<div class="scroll">
									<div id="tabel_seminar_proposal">

									</div>
							</div>
						</div>
						<div class="tab-pane fade" id="list-kolokium" role="tabpanel" aria-labelledby="list-kolokium-list">
							<div class="scroll">
								<div id="tabel_kolokium">

								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="list-yudisium" role="tabpanel" aria-labelledby="list-yudisium-list">
							<div class="scroll">
								<div id="tabel_yudisium">

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
</body>