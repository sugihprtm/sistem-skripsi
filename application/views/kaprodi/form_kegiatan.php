<script type="text/javascript">
	$('.clockpicker').clockpicker({
		placement: 'bottom',
		align: 'left',
		donetext: 'Done'
	});
	$(document).ready(function(){
		$("#save_kegiatan").on('submit',
			function(e) {
				e.preventDefault();
				var form = $(this);
				var formdata = false;

				if (window.FormData) {
					formdata = new FormData(form[0]);
				}

				var formAction = form.attr('action');

				$.ajax({
					type: 'POST',
					url: formAction,
					data: formdata ? formdata: form.serialize(),
					contentType: false,
					processData: false,
					cache: false,
					success: function() {
						swal("", "Pemberitahuan Kegiatan Berhasil Dikirim", "success");
					}
				});
			});
	});


	$('input[type="radio"]').click(function(){
		
		if($(this).attr("value")=="Sidang Kolokium"){
			$(".butt1").show('fast');
			$(".butt2").show('fast');
		}
		if($(this).attr("value")=="Sidang Yudisium"){
			$(".butt1").hide('fast');
			$(".butt2").hide('fast');
		}
		if($(this).attr("value")=="Seminar Proposal"){
			$(".butt1").show('fast');
			$(".butt2").hide('fast');
		}
	});
	$('input[type="radio"]').trigger('click'); 

/*	$(document).ready(function(){
		$('#penerima').change(function(){
			if($('#penerima').val()=='1'){
				$('#pertama').show();
			}
		})
		});
*/	
</script>
<form method="POST" id="save_kegiatan" action="<?php echo base_url('Kaprodi/aksi_kegiatan');?>">
	<div>
		<div class="form-row">
			<div class="form-group col-md">
				<label>Nama :</label>
				<select class="custom-select" id="penerima" name="penerima">
					<option selected>Pilih</option>
					<?php foreach ($mahasiswa as $m) {
						?>
						<option value="<?php echo $m->nim;?>"><?php echo $m->nama_mhs;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group col-md">
				<label> Tempat Kegiatan :</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-map-marker"></i></span>
					</div>
					<input type="text" name="tempat" class="form-control" required>
				</div>
			</div>
			<div class="form-group col-md">
				<label> Ruangan :</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fa fa-building"></i></span>
					</div>
					<input type="text" name="ruangan" class="form-control" required>
				</div>
			</div>

		</div>
		<div class="form-row">
			<div class="form-group col-md-8">
				<label>Tanggal Kegiatan</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="kalender"><i class="fas fa-calendar"></i></span>
					</div>
					<input aria-describedby="kalender" aria-label="Small" type="date" name="tanggal" class="form-control" required>
				</div>

			</div>
			<div class="form-group col-md clockpicker">
				<label>Jam Kegiatan</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="inputGroup-sizing-sm"><i class="fas fa-clock"></i></span>
					</div>
					<input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="jam" required>
				</div>
			</div>
		</div>

		<fieldset class="form-group">
			<div class="form-row">
				<!--<div class="col-md">
					<legend class="col-form-label col-md-sm-2 pt-0">Jenis Kegiatan :</legend>
					<select class="custom-select" id="inputGroupSelect02" name="jekeg">
					<option selected>Pilih</option>
					<option value="Seminar Proposal">Seminar Proposal</option>
					<option value="Sidang Kolokium">Sidang Kolokium</option>
					<option value="Sidang Yudisium">Seminar Yudisium</option>
					</select>
				</div> -->
				<div class="col-md">
					<legend class="col-form-label col-md-sm-2 pt-0">Jenis Kegiatan :</legend>
				</div>
				<div class="col-md">
					<div class="form-check">
						<input class="radio-inline" type="radio" name="jenis_kegiatan" value="Seminar Proposal">
						<label class="form-check-label" >
							Seminar Proposal
						</label>
					</div>
				</div>
				<div class="col-md">
					<div class="form-check">
						<input class="radio-inline" type="radio" name="jenis_kegiatan" value="Sidang Kolokium" checked>
						<label class="form-check-label" >
							Sidang Kolokium
						</label>
					</div>
				</div>
				<div class="col-md">
					<div class="form-check">
						<input class="radio-inline" type="radio" name="jenis_kegiatan" value="Sidang Yudisium">
						<label class="form-check-label" >
							Sidang Yudisium
						</label>
					</div>
				</div>
				
			</div>
			
		</fieldset>
		<div class="form-row">
		<div class="butt1 col-md" style="display: none">
				<label>Penguji 1</label>
				<select class="custom-select" id="penguji1" name="penguji1">
					<option selected></option>
					<?php foreach ($dosen as $d) {
						?>
						<option value="<?php echo $d->nama_dosen;?>"><?php echo $d->nama_dosen;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="butt2 col-md" style="display: none">
				<label>Penguji 2</label>
				<select class="custom-select" id="penguji2" name="penguji2">
					<option selected></option>
					<?php foreach ($dosen as $d) {
						?>
						<option value="<?php echo $d->nama_dosen;?>"><?php echo $d->nama_dosen;?></option>
					<?php } ?>
				</select>
			</div>
				
			</div>
			<br>
			<div class="col-md text-right">
					<button class="btn btn-primary"> Submit </button>
			</div>
			
	</div>
</form>