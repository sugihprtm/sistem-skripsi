<head>
	<script type="text/javascript" src="<?php echo base_url('assets/js/myscript.js');?>"></script>
	<script type="text/javascript">
		$(function(){

			$(document).on("click","div.small",function(){
				$(this).find("span[class~='caption']").hide();
				$(this).find("input[class~='editor']").fadeIn().focus();
			});

	</script>
</head>
<div class="table-responsive">
	<table class="table">
		<thead>
		<tr>
			<th scope="col">NIK</th>
			<th scope="col">Nama</th>
			<th scope="col">Prodi</th>
            <th scope="col"> </th>
		</tr>
		</thead>
		<tbody>
			<?php if (!empty($metlit)): foreach ($metlit as $u):
				?>
				<tr class="text-left list-item">
					<td><?php echo $u['nik_dosen_metlit'];?></td>
					<td><?php echo $u['nama_dosen'];?></td>
					<td><?php echo $u['prodi'];?></td>
                    <!--
                    <td><a id="<?php echo $u->id;?>" href="<?php echo base_url('Kaprodi/delete_metlit/').$u->id;?>" class="float-right hapus"><i class="fas fa-trash"> </i></a></td>
                	-->
				</tr>
				
			<?php endforeach; else: ?>
		
		</tbody>
	</table>
	<div class="col-md text-center">
		<p>Belum Ada Data.</p>	
	</div>
</div>

<?php endif; ?>
</div>